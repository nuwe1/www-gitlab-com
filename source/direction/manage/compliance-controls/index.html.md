---
layout: markdown_page
title: "Category Vision - Compliance Controls"
---

- TOC
{:toc}

## Compliance Controls: Introduction

Thanks for visiting the strategy page for Compliance Controls in GitLab. If you'd like to provide feedback on this page or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/720) for this category.

Enterprises operating in regulated environments need to ensure the technology they use complies with their internal company policies and procedures, which are largely based on the requirements of a particular legal or regulatory framework (e.g. GDPR, SOC 2, ISO, PCI-DSS, SOX, COBIT, HIPAA, FISMA, NIST) governing their industry. Customers need features that enable them to comply with these frameworks beginning with defining the rules, or controls, for their GitLab environment.

## Problems to Solve

Customers rely on organizations like GitLab to make the compliance process easier, often times by simply not making things more difficult or adding risk. Our goal is to provide "out-of-the-box" solutions where we can and customization where we can't. In order to do this, we must first establish the frameworks our customers adhere to so we can have a baseline to build upon.

* What success looks like: GitLab should provide framework-specific settings or policies that enable customers to quickly ensure the activities within their GitLab instance are conforming to organizational requirements. For example, Merge Requests should have a setting to toggle that introduces SOC2-focused requirements to meet before the MR can be approved.
 
When customers adhere to internal or external compliance frameworks, often times a need for customization arises. One organization's process for an activity can vary greatly from another's and this can create friction or usability issues. To facilitate better usability within GitLab towards compliance efforts, we'll be introducing features that enable customers to define specific policies or requirements their users and instance should abide by.

* What success looks like: Customers will be able to specify specific rules the instance must follow at an instance, group, project, and user-level to maintain tighter control of their environments. These rules should come from "standard" settings GitLab provides and provide an option to customize those rules as necessary to suit individual customer needs.

In almost all cases, Compliance Controls for an organization focus on reducing overall risk, enforcing separation of duties, and implementing remediation processes. Without features to support these efforts, administrators for these organizations are faced with few workarounds: they can tolerate higher risk, lock down an instance and manage additional administrative overhead at the price of velocity, or build and maintain layers of automation on their own. For enterprises to thrive, this is a problem that demands a high-quality, native solution in GitLab.

* What success looks like: Customers should be able to transfer internal company policies to GitLab's environment to meet their needs, but without the complexity and headache of massive configuration time.

## What's Next & Why

Organizations need control of all technology they use to mitigate risk and adhere to compliance frameworks. GitLab's Compliance Controls category aims to address this need by introducing features and experiences that enable our customers to introduce better controls for their instances.

There will never be a perfect set of controls for every organization. For example, we've learned from [user feedback](https://docs.google.com/document/d/1-MZItci-Uxn4m-uEh5wR5VnC2sTeR_8UYdgaZmO6Yik/edit) that granular user roles and permissions are a very difficult, nuanced problem to solve. Organizations want customizable, deep control on a wide swath of settings and features.  It's possible for us to iterate on "sensible defaults" to make setting up GitLab environments faster and easier; it's also feasible for us to introduce enforcement capabilities to make managing compliance with GitLab more credible and effective.

This category - Compliance Controls - is designed to introduce better control at the [project](https://gitlab.com/groups/gitlab-org/-/epics/2087) level with features like [controls definition](https://gitlab.com/gitlab-org/gitlab/issues/36300) and [compliance checks in merge requests](https://gitlab.com/gitlab-org/gitlab/issues/34830). It also focuses on [group-level](https://gitlab.com/groups/gitlab-org/-/epics/2187) considerations such as [preventing project maintainers from changing critical settings](https://gitlab.com/gitlab-org/gitlab/issues/38051) like merge request approvals.

We're also looking at an additional layer of user access control in our [Policies MVC](https://gitlab.com/gitlab-org/gitlab/issues/7626) to extend user permission functionality. We're inspired by [identity-based policies in systems like AWS](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies.html#policies_id-based) that allow policies to be defined and applied to resources we'd like to protect.

By introducing a layer of enforcement on top of our existing [RBAC](https://en.wikipedia.org/wiki/Role-based_access_control), this system lends us a high degree of flexibility in GitLab and reduces risk by adding to our permissions system instead of supplanting it.

The first step on this journey is introducing a Minimal, viable version of Compliance Controls - described further in our Maturity Plan below.

### How you can help

As we're still the conceptual stages of developing Compliance Controls, **your feedback is critical** to successfully shaping this category so that it works for your organization's needs. If you're interested in this problem space, please [comment on your needs and experiences with permissions in GitLab](https://gitlab.com/groups/gitlab-org/-/epics/1982). We're especially interested in:
* How are you currently working around permissions and compliance problems in GitLab?
* What specific controls would you like introduced?
* Which conditions would ideally determine which users are not able to take a particular action? Conversely, which conditions would ideally determine who is?
* Do you have any comments on the solutions we're exploring?

### [Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/720)

Compliance Controls is currently **Planned**. The next step for Compliance Controls' is **Minimal** maturity. 
* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.

To achieve a minimal version of Compliance Controls, we'll conduct deep discovery on the problem space before taking our first step. An [MVC](https://about.gitlab.com/handbook/product/#the-minimally-viable-change-mvc) for Compliance Controls will likely allow an administrator or Owner to define "sensible defaults" and apply the ruleset to a particular project.
* Please track the Minimal maturity plan in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/1982).

Once we've achieved a minimal version of Compliance Controls, achieving a **Viable** level of maturity will involve collecting customer feedback and reacting. Assuming we're on the right track, we'll likely scale the solution in two dimensions:
* Increase the number of settings and features you can affect with Compliance Controls. The MVC will likely focus on 1-2 common compliance use cases, and we'll need to increase the breadth of controls offered,
* Increase the scope of Compliance Controls to other features of GitLab, such as user roles and permissions, pipelines, and automatica issue creation for remediation tasks.

Finally, once we've achieved a ruleset that's sufficiently flexible and powerful for enterprises, it's not enough to be able to define these rules - we should be able to measure and confirm that they're being adhered to. Achieving **Complete** or **Lovable** maturity likely means further expansion of the two dimensions above, plus visualizing/reporting on the state of Compliance Controls across the instance.
