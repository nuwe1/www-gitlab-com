---
layout: markdown_page
title: "Category Vision - Importers"
---

- TOC
{:toc}

## Importers

Thanks for visiting the strategy page for Importers in GitLab. If you'd like to provide feedback on this page or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/2248) for this category.

## What's Next & Why

At the moment, the group is focused on enabling GitLab.com adoption through the introduction of [group import/export](https://gitlab.com/groups/gitlab-org/-/epics/1952). More details on this category's vision are currently under construction.

## Maturity Plan

Our maturity plan is currently under construction. If you'd like to contribute feedback on areas you'd like to see prioritized, please add them as comments in the [epic](https://gitlab.com/groups/gitlab-org/-/epics/2248).
